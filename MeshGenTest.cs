using Godot;
using Godot.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using PaperMartin.Utils.Maths;
using System.Threading.Tasks;
using VoxelGame.Data;

public class MeshGenTest : Spatial
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private MeshInstance meshInstance;
	private GIProbe probes;
	
	public Vector3Int Blocks = new Vector3Int(16, 16, 16);
	[Export] public float BlockSize;

	[Export] public Chunk chunk;

	private struct BlockBuildingData
	{
		public float blockSize;
		public Vector3Int blockPos;
		public List<Vector3> verts;
		public List<Vector2> uvs;
		public List<Vector3> normals;
		public List<Color> colors;
		public System.Threading.Mutex mutex;
	}

	private Task CurrentMeshUpdateTask;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

	public Task StartUpdateMesh()
	{
		if (CurrentMeshUpdateTask != null)
		{
			if (CurrentMeshUpdateTask.Status == TaskStatus.Running)
			{
				CurrentMeshUpdateTask.Wait();
			}
		}

		CurrentMeshUpdateTask = Task.Run(() => ThreadedUpdateMesh());
		return CurrentMeshUpdateTask;
	}

	public void ForceFinishUpdateMesh()
	{
		CurrentMeshUpdateTask.Wait();
	}

	public void UpdateLighting()
	{
		probes = GetNode<GIProbe>("GIProbe");
		Vector3 Extents = (Blocks.ToVector3() * BlockSize) / 2;
		probes.Extents = Extents;
		Vector3 translation = Extents;
		translation.z = -translation.z;
		probes.Translation = translation;
		probes.Bake();
	}

	public void ClearMesh()
	{
		if (CurrentMeshUpdateTask.Status == TaskStatus.Running)
		{
			CurrentMeshUpdateTask.Wait();
		}
		meshInstance = GetNode<MeshInstance>("MeshInstance");
		meshInstance.Mesh = null;
	}

	public void ThreadedUpdateMesh()
	{
		meshInstance = GetNode<MeshInstance>("MeshInstance");
		Vector3 Zero = new Vector3(0, 0, 0);

		Task[] tasks = new Task[Blocks.x * Blocks.y * Blocks.z];

		ArrayMesh voxelmesh = new ArrayMesh();

		List<Vector3> finalverts = new List<Vector3>();
		List<Vector2> finalUVs = new List<Vector2>();
		List<Vector3> finalNormals = new List<Vector3>();
		List<Color> finalColors = new List<Color>();

		float taskcreationstart = OS.GetTicksMsec();

		int CurrentID = 0;
		for (int x = 0; x < Blocks.x; x++)
		{
			for (int y = 0; y < Blocks.y; y++)
			{
				for (int z = 0; z < Blocks.z; z++)
				{
					BlockBuildingData data = new BlockBuildingData();
					data.verts = finalverts;
					data.uvs = finalUVs;
					data.normals = finalNormals;
					data.colors = finalColors;
					data.blockSize = BlockSize;
					data.blockPos = new Vector3Int(x, y, -z);
					AddBlock(data, chunk);
					//tasks[CurrentID] = newTask;
					CurrentID += 1;
				}
			}
		}

		float taskcreationend = OS.GetTicksMsec();
		//GD.Print("task creation : " + (taskcreationend - taskcreationstart) + "ms");

		float meshfinalisationstart = OS.GetTicksMsec();
		Godot.Collections.Array arr = new Godot.Collections.Array();
		arr.Resize((int) ArrayMesh.ArrayType.Max);

		//Task.WaitAll(tasks);

		if (finalverts.Count != 0)
		{
			arr[(int) ArrayMesh.ArrayType.Vertex] = finalverts;
			arr[(int) ArrayMesh.ArrayType.TexUv] = finalUVs;
			arr[(int) ArrayMesh.ArrayType.Normal] = finalNormals;
			arr[(int) ArrayMesh.ArrayType.Color] = finalColors;

			voxelmesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, arr);
		}
		voxelmesh.RegenNormalmaps();
		meshInstance.Mesh = voxelmesh;
		float meshfinalisationend = OS.GetTicksMsec();
		//GD.Print("mesh finalisation : " + (taskcreationend - taskcreationstart) + "ms");
	}

	void AddBlock(BlockBuildingData blockData, Chunk chunkData)
	{
		Vector3Int unnegatedblockPos = blockData.blockPos;
		unnegatedblockPos.z = -unnegatedblockPos.z;

		Tile CurrentTile = chunkData.GetTile(unnegatedblockPos);

		if (CurrentTile != null)
		{
			if (CurrentTile.IsBlock)
			{
				Vector3Int UnNegatedBlockPos = blockData.blockPos;
				UnNegatedBlockPos.z = -UnNegatedBlockPos.z;
				
				//Forward quad
				Vector3Int ForwardPos = UnNegatedBlockPos;
				ForwardPos.z += 1;

				Tile ForwardTile = chunkData.GetTile(ForwardPos);

				if (ForwardTile != null)
				{
					if (ForwardTile.OccludesAdjacentTiles == false)
					{
						AddQuad(blockData.blockPos, Vector3.Up, 0, BlockSize, blockData);
					}
				}
				else
				{
					AddQuad(blockData.blockPos, Vector3.Up, 0, BlockSize, blockData);
				}

				//Left quad
				Vector3Int LeftPos = UnNegatedBlockPos;
				LeftPos.x -= 1;

				Tile LeftTile = chunkData.GetTile(LeftPos);
				
				if (LeftTile != null)
				{
					if (LeftTile.OccludesAdjacentTiles == false)
					{
						AddQuad(blockData.blockPos, Vector3.Up, (float) Math.PI / 2, blockData.blockSize, blockData);
					}
				}
				else
				{
					AddQuad(blockData.blockPos, Vector3.Up, (float) Math.PI / 2, blockData.blockSize, blockData);
				}
				
				//Right quad
				Vector3Int RightPos = UnNegatedBlockPos;
				RightPos.x += 1;

				Tile RightTile = chunkData.GetTile(RightPos);
				
				if (RightTile != null)
				{
					if (RightTile.OccludesAdjacentTiles == false)
					{
						AddQuad(blockData.blockPos, Vector3.Up, (float) Math.PI * 1.5f, blockData.blockSize, blockData);
					}
				}
				else
				{
					AddQuad(blockData.blockPos, Vector3.Up, (float) Math.PI * 1.5f, blockData.blockSize, blockData);
				}
				
				//Backwards Quad
				Vector3Int BackPos = UnNegatedBlockPos;
				BackPos.z -= 1;

				Tile BackTile = chunkData.GetTile(BackPos);
				
				if (BackTile != null)
				{
					if (BackTile.OccludesAdjacentTiles == false)
					{
						AddQuad(blockData.blockPos, Vector3.Up, (float) Math.PI, blockData.blockSize, blockData);
					}
				}
				else
				{
					AddQuad(blockData.blockPos, Vector3.Up, (float) Math.PI, blockData.blockSize, blockData);
				}

				//Down quad
				Vector3Int DownPos = UnNegatedBlockPos;
				DownPos.y -= 1;

				Tile DownTile = chunkData.GetTile(DownPos);
				
				if (DownTile != null)
				{
					if (DownTile.OccludesAdjacentTiles == false)
					{
						AddQuad(blockData.blockPos, Vector3.Left, (float) Math.PI / 2, blockData.blockSize, blockData);
					}
				}
				else
				{
					AddQuad(blockData.blockPos, Vector3.Left, (float) Math.PI / 2, blockData.blockSize, blockData);
				}
				
				//Up quad
				Vector3Int UpPos = UnNegatedBlockPos;
				UpPos.y += 1;

				Tile UpTile = chunkData.GetTile(UpPos);
				
				if (UpTile != null)
				{
					if (UpTile.OccludesAdjacentTiles == false)
					{
						AddQuad(blockData.blockPos, Vector3.Left, -(float) Math.PI / 2, blockData.blockSize, blockData);
					}
				}
				else
				{
					AddQuad(blockData.blockPos, Vector3.Left, -(float) Math.PI / 2, blockData.blockSize, blockData);
				}
			}
		}
	}


	void AddQuad(Vector3Int Position, Vector3 Axis, float Rotation, float Size, BlockBuildingData data)
	{
		Vector3 BottomRight = new Vector3(0, 0, -1);
		Vector3 BottomLeft = new Vector3(1, 0, -1);
		Vector3 TopRight = new Vector3(0, 1, -1);
		Vector3 TopLeft = new Vector3(1, 1, -1);
		Vector3 half = new Vector3(0.5f, 0.5f, -0.5f);
		Vector3 Direction = Vector3.Forward.Rotated(Axis, Rotation);

		BottomLeft -= half;
		BottomLeft = BottomLeft.Rotated(Axis, Rotation);
		BottomLeft += half;

		BottomRight -= half;
		BottomRight = BottomRight.Rotated(Axis, Rotation);
		BottomRight += half;

		TopLeft -= half;
		TopLeft = TopLeft.Rotated(Axis, Rotation);
		TopLeft += half;

		TopRight -= half;
		TopRight = TopRight.Rotated(Axis, Rotation);
		TopRight += half;

		BottomLeft *= Size;
		BottomRight *= Size;
		TopLeft *= Size;
		TopRight *= Size;
		
		Color VertexColor = new Color(Direction.x,Direction.y,Direction.z);

		Vector3 FloatPos = new Vector3(Position.x, Position.y, Position.z) * BlockSize;
		
		data.colors.Add(VertexColor);
		data.uvs.Add(new Vector2(0, 1));
		data.verts.Add(BottomLeft + FloatPos);
		data.normals.Add(Direction);

		data.colors.Add(VertexColor);
		data.uvs.Add(new Vector2(0, 0));
		data.verts.Add(TopLeft + FloatPos);
		data.normals.Add(Direction);

		data.colors.Add(VertexColor);
		data.uvs.Add(new Vector2(1, 0));
		data.verts.Add(TopRight + FloatPos);
		data.normals.Add(Direction);


		data.colors.Add(VertexColor);
		data.uvs.Add(new Vector2(1, 0));
		data.verts.Add(TopRight + FloatPos);
		data.normals.Add(Direction);

		data.colors.Add(VertexColor);
		data.uvs.Add(new Vector2(1, 1));
		data.verts.Add(BottomRight + FloatPos);
		data.normals.Add(Direction);

		data.colors.Add(VertexColor);
		data.uvs.Add(new Vector2(0, 1));
		data.verts.Add(BottomLeft + FloatPos);
		data.normals.Add(Direction);
	}
	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	//  public override void _Process(float delta)
	//  {
	//      
	//  }
}
