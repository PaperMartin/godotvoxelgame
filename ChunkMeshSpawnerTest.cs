using Godot;
using Godot.Collections;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using PaperMartin.Utils.Maths;
using System.Threading.Tasks;
using VoxelGame.Data;
using VoxelGame.Generation;

public class ChunkMeshSpawnerTest : Spatial
{
    [Export] private ChunkGenerator generator;

    [Export] private DimensionSettings DimSettings;

    [Export] private LevelSettings levelSettings;

    [Export] private PackedScene ChunkScene;

    private ConcurrentDictionary<Vector3Int, Chunk> chunks;
    private MeshGenTest[,,] MGTs;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        SpawnChunks();
    }

    private void SpawnChunks()
    {
        foreach (Node childnode in GetChildren())
        {
            childnode.QueueFree();
        }


        Vector3Int ChunkAmountInt = DimSettings.GetDimensionSize();
        Vector3Int BlockPerChunk = levelSettings.GetChunkSize();

        MGTs = new MeshGenTest[ChunkAmountInt.x, ChunkAmountInt.y, ChunkAmountInt.z];

        int initialCapacity = ChunkAmountInt.x * ChunkAmountInt.y * ChunkAmountInt.z;

        int numProcs = System.Environment.ProcessorCount;
        int concurrencyLevel = numProcs * 2;
        
        chunks = new ConcurrentDictionary<Vector3Int, Chunk>(concurrencyLevel, initialCapacity);

        Vector3Int chunkCoords = new Vector3Int(0, 0, 0);

        float StartTime = OS.GetTicksMsec();
        RandomNumberGenerator rng = new RandomNumberGenerator();
        rng.Randomize();
        int seed = rng.RandiRange(0, 99999);

        Vector3Int BlocksInt = new Vector3Int(
            Mathf.RoundToInt(BlockPerChunk.x),
            Mathf.RoundToInt(BlockPerChunk.y),
            Mathf.RoundToInt(BlockPerChunk.z));

        Task[] worldGenTasks = new Task[ChunkAmountInt.x * ChunkAmountInt.y * ChunkAmountInt.z];

        int CurrentTaskIndex = 0;

        for (chunkCoords.x = 0; chunkCoords.x < ChunkAmountInt.x; chunkCoords.x++)
        {
            for (chunkCoords.y = 0; chunkCoords.y < ChunkAmountInt.y; chunkCoords.y++)
            {
                for (chunkCoords.z = 0; chunkCoords.z < ChunkAmountInt.z; chunkCoords.z++)
                {
                    Vector3Int chunkCoordsCopy = chunkCoords;

                    worldGenTasks[CurrentTaskIndex] = Task.Run(() =>
                    {
                        //Chunk chunk = generator.GenerateChunk(chunkCoordsCopy, levelSettings, seed);
                        //chunks[chunkCoordsCopy] = chunk;
                    });

                    Vector3 chunkPos = new Vector3(chunkCoords.x, chunkCoords.y, -chunkCoords.z);
                    MeshGenTest newChunk = SpawnSingleChunk(chunkPos);
                    MGTs[chunkCoords.x, chunkCoords.y, chunkCoords.z] = newChunk;
                    CurrentTaskIndex += 1;
                }
            }
        }

        Task.WaitAll(worldGenTasks);

        for (chunkCoords.x = 0; chunkCoords.x < ChunkAmountInt.x; chunkCoords.x++)
        {
            for (chunkCoords.y = 0; chunkCoords.y < ChunkAmountInt.y; chunkCoords.y++)
            {
                for (chunkCoords.z = 0; chunkCoords.z < ChunkAmountInt.z; chunkCoords.z++)
                {
                    MGTs[chunkCoords.x, chunkCoords.y, chunkCoords.z].chunk = chunks[chunkCoords];
                }
            }
        }

        float EndTime = OS.GetTicksMsec();
        GD.Print("World Gen & Chunk Node Spawning : " + (EndTime - StartTime));
        GD.Print("/////");
    }

    public void UpdateChunks()
    {
        float StartTime = OS.GetTicksMsec();

        List<Task> tasks = new List<Task>();

        foreach (MeshGenTest MGT in GetChildren())
        {
            MGT.StartUpdateMesh();
        }

        float EndTime = OS.GetTicksMsec();
        GD.Print("Exec Time : " + (EndTime - StartTime));
        GD.Print("/////");
    }

    public void UpdateChunksInstant()
    {
        float StartTime = OS.GetTicksMsec();

        foreach (MeshGenTest MGT in GetChildren())
        {
            MGT.StartUpdateMesh();
        }

        foreach (MeshGenTest MGT in GetChildren())
        {
            MGT.ForceFinishUpdateMesh();
        }

        /*foreach (MeshGenTest MGT in GetChildren())
        {
            MGT.UpdateLighting();
        }*/

        float EndTime = OS.GetTicksMsec();
        GD.Print("Exec Time : " + (EndTime - StartTime));
        GD.Print("/////");
    }

    public void ClearMeshes()
    {
        foreach (MeshGenTest MGT in GetChildren())
        {
            MGT.ClearMesh();
        }
    }

    private MeshGenTest SpawnSingleChunk(Vector3 Pos)
    {
        Vector3Int ChunkAmountInt = DimSettings.GetDimensionSize();
        Vector3Int BlockPerChunk = levelSettings.GetChunkSize();
        float BlockSize = levelSettings.GetBlockSize();
        
        Vector3 ChunkPos = Pos;
        ChunkPos *= BlockSize;
        ChunkPos.x *= BlockPerChunk.x;
        ChunkPos.y *= BlockPerChunk.y;
        ChunkPos.z *= BlockPerChunk.z;

        MeshGenTest Chunk = (MeshGenTest)ChunkScene.Instance();
        Chunk.Translation = ChunkPos;

        Vector3Int BlocksInt = new Vector3Int(
            Mathf.RoundToInt(BlockPerChunk.x),
            Mathf.RoundToInt(BlockPerChunk.y),
            Mathf.RoundToInt(BlockPerChunk.z));

        Chunk.Blocks = BlocksInt;
        Chunk.BlockSize = BlockSize;
        AddChild(Chunk);
        Chunk.Owner = this;
        Chunk.Name = "Chunk " + Pos;
        return Chunk;
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}