using Godot;
using System;
using System.Collections.Generic;

namespace PaperMartin.Utils.Maths
{
    public struct Vector3Int : IEquatable<Vector3Int>
    {

        public int x;
        public int y;
        public int z;

        public Vector3Int(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3 ToVector3()
        {
            Vector3 v = new Vector3();
            v.x = x;
            v.y = y;
            v.z = z;
            return v;
        }

        private sealed class XYZEqualityComparer : IEqualityComparer<Vector3Int>
        {
            public bool Equals(Vector3Int x, Vector3Int y)
            {
                return x.x == y.x && x.y == y.y && x.z == y.z;
            }

            public int GetHashCode(Vector3Int obj)
            {
                unchecked
                {
                    var hashCode = obj.x;
                    hashCode = (hashCode * 397) ^ obj.y;
                    hashCode = (hashCode * 397) ^ obj.z;
                    return hashCode;
                }
            }
        }

        public static IEqualityComparer<Vector3Int> XYZComparer { get; } = new XYZEqualityComparer();

        public bool Equals(Vector3Int other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public override bool Equals(object obj)
        {
            return obj is Vector3Int other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = x;
                hashCode = (hashCode * 397) ^ y;
                hashCode = (hashCode * 397) ^ z;
                return hashCode;
            }
        }   
    }

    public static class Vector3Extensions
    {
        public static Vector3Int RoundToInt(this Vector3 v3)
        {
            Vector3Int v3int = new Vector3Int(Mathf.RoundToInt(v3.x), Mathf.RoundToInt(v3.y), Mathf.RoundToInt(v3.z));
            return v3int;
        }
    }
}