using Godot;
using System;
using VoxelGame.Data;
using VoxelGame.Generation;
using VoxelGame.Runtime;

public class MapGenHUD : Control
{
    [Export()] private NodePath DimInstance;
    [Export()] private NodePath ProgBar;

    private DimensionInstance dimInst;
    private ProgressBar progBar;
    
    
    public override void _Ready()
    {
        dimInst = (DimensionInstance)GetNode(DimInstance);
        progBar = (ProgressBar)GetNode(ProgBar);
    }

    public override void _Process(float delta)
    {
        float GenProgress = dimInst.GetGenerationProgress();
        progBar.Value = GenProgress;
    }

    public void GenerateMap()
    {
        dimInst.GenerateWorld();
    }

    public void GenerateMesh()
    {
        dimInst.GenerateChunkMeshes();
    }

    public void ClearWorld()
    {
        dimInst.ResetWorld();
    }
    
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
