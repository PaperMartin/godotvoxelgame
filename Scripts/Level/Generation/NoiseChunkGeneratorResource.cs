using Godot;
using System;
using VoxelGame.Generation;
using VoxelGame.Data;

public class NoiseChunkGeneratorResource : ChunkGeneratorResource
{
    [Export] public int Octaves = 1;
    [Export] public float Period = 10;
    [Export] public float Persistence = 0.5f;
    [Export] public float Lacunarity = 2;

    [Export] public int GroundMinHeight = 16;
    [Export] public int GroundMaxHeight = 20;

    [Export] public int GroundStepCount = 3;
        
        
    [Export(PropertyHint.Range,"0.0,1.0")] public float GroundStepInfluence = 0.5f;

    [Export(PropertyHint.Range,"0.0,1.0")] public float caveStep = 0.5f;

    [Export] public Tile SolidTile;

    [Export] public Tile VoidTile;

    [Export] public float Scale;
    
    public override ChunkGenerator GetGeneratorInstance()
    {
        NoiseChunkGenerator generator = new NoiseChunkGenerator();
        generator.Resource = this;
        return generator;
    }
}
