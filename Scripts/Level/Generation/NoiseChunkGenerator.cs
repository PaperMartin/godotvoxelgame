using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using PaperMartin.Utils.Maths;
using System.Threading.Tasks;
using VoxelGame.Data;

namespace VoxelGame.Generation
{
    public class NoiseChunkGenerator : ChunkGenerator
    {
        private OpenSimplexNoise noise = new OpenSimplexNoise();

        public NoiseChunkGeneratorResource Resource;


        private Task MapGenTask;

        private System.Threading.Mutex TaskListMutex = new System.Threading.Mutex();
        List<Task> ChunkGenTasks = new List<Task>();

        public override void GenerateChunk(Vector3Int chunkCoords, int seed)
        {
            LevelSettings levelSettings = dimension.LevelSettings;
            Chunk chunk = dimension.GetChunk(chunkCoords);

            Vector3Int currentcoord = new Vector3Int(0, 0, 0);

            Vector3Int chunkSize = levelSettings.GetChunkSize(); 

            for (currentcoord.x = 0; currentcoord.x < chunkSize.x; currentcoord.x++)
            {
                for (currentcoord.y = 0; currentcoord.y < chunkSize.y; currentcoord.y++)
                {
                    for (currentcoord.z = 0; currentcoord.z < chunkSize.z; currentcoord.z++)
                    {
                        SetTileAtCoord(chunkCoords, currentcoord, levelSettings, chunk);
                    }
                }
            }
        }

        public override void StartGeneratingMap(int seed)
        {
            if (MapGenTask != null)
            {
                if (MapGenTask.IsCompleted != true)
                {
                    MapGenTask.Wait();
                }
            }

            MapGenTask = Task.Run(() =>
            {
                ThreadedMapGen(dimension, seed);
            });
        }

        public override float GetProgress()
        {
            TaskListMutex.WaitOne();
            if (ChunkGenTasks != null && ChunkGenTasks.Count != 0)
            {
                int TasksCompleted = 0;
                
                int TotalTasks = ChunkGenTasks.Count;
                foreach (Task task in ChunkGenTasks )
                {
                    if (task.IsCompleted)
                    {
                        TasksCompleted += 1;
                    }
                }
                //GD.Print("Tasks Completed : " + TasksCompleted + " Total Tasks : " + TotalTasks);
                TaskListMutex.ReleaseMutex();
                return (float)TasksCompleted / (float)TotalTasks;
            }
            TaskListMutex.ReleaseMutex();

            return 1f;
        }

        public override void WaitForCompletion()
        {
            if (MapGenTask != null)
            {
                MapGenTask.Wait();
            }
        }

        private void ThreadedMapGen(Dimension dimension, int seed)
        {
            float GenStart = OS.GetTicksMsec();
            GD.Print("Starting map generation");
            dimension.mutex.WaitOne();
            
            TaskListMutex.WaitOne();
            ChunkGenTasks.Clear();
            TaskListMutex.ReleaseMutex();
            
            noise.Octaves = Resource.Octaves;
            noise.Period = Resource.Period;
            noise.Persistence = Resource.Persistence;
            noise.Lacunarity = Resource.Lacunarity;
            noise.Seed = seed;
            
            DimensionSettings dimensionSettings = dimension.Settings;
            LevelSettings levelSettings = dimension.LevelSettings;
            Vector3Int DimSize = dimensionSettings.GetDimensionSize();
            Vector3Int ChunkSize = levelSettings.GetChunkSize();
            
            float blockSize = levelSettings.GetBlockSize();

            TaskListMutex.WaitOne();
            for (int x = 0; x < DimSize.x; x++)
            {
                for (int y = 0; y < DimSize.y; y++)
                {
                    for (int z = 0; z < DimSize.z; z++)
                    {
                        Vector3Int coord = new Vector3Int(x, y, z);
                        Task chunkTask = Task.Run(() => { GenerateChunk(coord, seed); });
                        ChunkGenTasks.Add(chunkTask);
                    }
                }
            }
            TaskListMutex.ReleaseMutex();
            GD.Print("Chunks to generate : " + ChunkGenTasks.Count);

            Task.WaitAll(ChunkGenTasks.ToArray());
            float GenFinish = OS.GetTicksMsec();
            GD.Print("Finishing map generation : " + (GenFinish - GenStart));

            dimension.mutex.ReleaseMutex();
        }

        private void SetTileAtCoord(Vector3Int chunkCoords, Vector3Int coords, LevelSettings levelSettings,
            Chunk chunk)
        {
            Vector3Int chunkSize = levelSettings.GetChunkSize();
            float blockSize = levelSettings.GetBlockSize();
            
            Vector3 v3ChunkCoords = chunkCoords.ToVector3();
            Vector3 v3Coords = coords.ToVector3();
            v3Coords += v3ChunkCoords * chunkSize.ToVector3();
            v3Coords *= blockSize;
            Vector3 v3CoordsScaled = v3Coords * Resource.Scale;

            float GroundHeight = 16;

            float groundnoiseresult = noise.GetNoise2d(v3CoordsScaled.x, v3CoordsScaled.z);
            groundnoiseresult += 1;
            groundnoiseresult *= 0.5f;
            groundnoiseresult = Mathf.Stepify(groundnoiseresult, (1.0f / Resource.GroundStepCount));

            float groundnoiseresult2 = noise.GetNoise2d((v3CoordsScaled.x * 2) + 1000, (v3CoordsScaled.z * 2) + 1000);
            groundnoiseresult2 += 1;
            groundnoiseresult2 *= 0.5f;

            groundnoiseresult = Mathf.Lerp(groundnoiseresult, groundnoiseresult2, Resource.GroundStepInfluence);

            GroundHeight = Mathf.Lerp((float) Resource.GroundMinHeight, (float) Resource.GroundMaxHeight, groundnoiseresult);
            //GD.Print(GroundHeight);
            //GD.Print(v3Coords);

            float cavenoiseresult = noise.GetNoise3d(v3CoordsScaled.x, v3CoordsScaled.y, v3CoordsScaled.z);

            if (cavenoiseresult > Resource.caveStep)
            {
                //GD.Print(v3Coords + " : Void " + Mathf.Stepify(noiseresult, 0.01f));
                chunk.SetTile(coords, Resource.VoidTile);
            }

            else if (v3Coords.y > GroundHeight)
            {
                //GD.Print(v3Coords + " : Void " + Mathf.Stepify(noiseresult, 0.01f));
                chunk.SetTile(coords, Resource.VoidTile);
            }
            else
            {
                //GD.Print(v3Coords + " : Solid" + Mathf.Stepify(noiseresult, 0.01f));
                chunk.SetTile(coords, Resource.SolidTile);
            }
        }

        public override void OnDimensionSpawn()
        {
            
        }

        public override void OnDimensionDespawn()
        {
            
        }

        public override void OnComponentAttached()
        {
            
        }

        public override void OnComponentRemoved()
        {
            
        }
    }
}