using Godot;
using System;
using VoxelGame.Data;
using VoxelGame.Generation;

public class FlatChunkGeneratorResource : ChunkGeneratorResource
{
    [Export()] public int GroundHeight;
    [Export()] public Tile GroundTile;
    [Export()] public Tile AirTile;
    public override ChunkGenerator GetGeneratorInstance()
    {
        FlatChunkGenerator generator = new FlatChunkGenerator();
        generator.Resource = this;
        return generator;
    }
}
