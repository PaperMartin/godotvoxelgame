using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaperMartin.Utils.Maths;
using VoxelGame.Data;


namespace VoxelGame.Generation
{
    public abstract class ChunkGenerator : DimensionComponent
    {
        public abstract void GenerateChunk(Vector3Int chunkCoords, int seed);

        public abstract void StartGeneratingMap(int seed);

        public abstract float GetProgress();
        
        public abstract void WaitForCompletion();
    }
}