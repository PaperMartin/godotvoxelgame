using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using PaperMartin.Utils.Maths;
using System.Threading.Tasks;
using VoxelGame.Data;
using VoxelGame.Generation;
using System.Linq;
using System.Threading;

public class FlatChunkGenerator : ChunkGenerator
{
    private OpenSimplexNoise noise = new OpenSimplexNoise();

    public FlatChunkGeneratorResource Resource;


    private Task MapGenTask;

    private System.Threading.Mutex TaskProgressMutex = new System.Threading.Mutex();

    private int CurrentProgress;
    private int TotalProgress;

    public override void GenerateChunk(Vector3Int chunkCoords, int seed)
    {
        LevelSettings levelSettings = dimension.LevelSettings;
        Chunk chunk = dimension.GetChunk(chunkCoords);
        chunk.mutex.WaitOne();
        
        Vector3Int currentcoord = new Vector3Int(0, 0, 0);

        Vector3Int chunkSize = levelSettings.GetChunkSize();

        for (currentcoord.x = 0; currentcoord.x < chunkSize.x; currentcoord.x++)
        {
            for (currentcoord.y = 0; currentcoord.y < chunkSize.y; currentcoord.y++)
            {
                for (currentcoord.z = 0; currentcoord.z < chunkSize.z; currentcoord.z++)
                {
                    SetTileAtCoord(chunkCoords, currentcoord, levelSettings, chunk);
                }
            }
        }
        chunk.mutex.ReleaseMutex();
    }

    public override void StartGeneratingMap(int seed)
    {
        if (MapGenTask != null)
        {
            if (MapGenTask.IsCompleted != true)
            {
                MapGenTask.Wait();
            }
        }

        MapGenTask = Task.Run(() => { ThreadedMapGen(dimension, seed); });
    }

    public override float GetProgress()
    {
        //TaskProgressMutex.WaitOne();
        if (TotalProgress != 0)
        {
            float progress = (float)CurrentProgress / (float)TotalProgress;

            //GD.Print("Tasks Completed : " + TasksCompleted + " Total Tasks : " + TotalTasks);
            //TaskProgressMutex.ReleaseMutex();
            return progress;
        }

        //TaskProgressMutex.ReleaseMutex();

        return 1f;
    }

    public override void WaitForCompletion()
    {
        if (MapGenTask != null)
        {
            MapGenTask.Wait();
        }
    }

    private void ThreadedMapGen(Dimension dimension, int seed)
    {
        float GenStart = OS.GetTicksMsec();
        GD.Print("Starting map generation");

        DimensionSettings dimensionSettings = dimension.Settings;
        Vector3Int DimSize = dimensionSettings.GetDimensionSize();

        TaskProgressMutex.WaitOne();
        CurrentProgress = 0;
        TotalProgress = DimSize.x * DimSize.y * DimSize.z;
        TaskProgressMutex.ReleaseMutex();

        int ChunkAmount = DimSize.x * DimSize.y * DimSize.z;
        GD.Print("Chunks to generate : " + ChunkAmount);

        Vector3[] AllChunkCoords = dimension.GetAllCoords().ToArray();

        ParallelOptions options = new ParallelOptions();
        options.TaskScheduler = WorldGenTaskScheduler.GetInstance();

        ParallelLoopResult result = Parallel.For(0, ChunkAmount,options, (int i) =>
        {
            //GD.Print("Current iteration : " + i);
            Vector3Int ChunkCoords = AllChunkCoords[i].RoundToInt();
            
            //GD.Print("Current Chunk : " + ChunkCoords.x  + " " + ChunkCoords.y  + " " + ChunkCoords.z );

            GenerateChunk(ChunkCoords, seed);

            Interlocked.Increment(ref CurrentProgress);
            //GD.Print("Chunk " + ChunkCoords.x  + " " + ChunkCoords.y  + " " + ChunkCoords.z + " finished generating. Current Progress : " + CurrentProgress);

        });

        float GenFinish = OS.GetTicksMsec();
        GD.Print("Finishing map generation : " + (GenFinish - GenStart));
    }

    private void SetTileAtCoord(Vector3Int chunkCoords, Vector3Int coords, LevelSettings levelSettings,
        Chunk chunk)
    {
        Vector3Int chunkSize = levelSettings.GetChunkSize();
        float blockSize = levelSettings.GetBlockSize();
        Vector3 v3ChunkCoords = chunkCoords.ToVector3();
        Vector3 v3Coords = coords.ToVector3();
        v3Coords += v3ChunkCoords * chunkSize.ToVector3();

        //v3Coords *= blockSize;

        if (v3Coords.y > Resource.GroundHeight)
        {
            //GD.Print(v3Coords + " : Void " + Mathf.Stepify(noiseresult, 0.01f));
            chunk.SetTile(coords, Resource.AirTile);
        }
        else
        {
            //GD.Print(v3Coords + " : Solid" + Mathf.Stepify(noiseresult, 0.01f));
            chunk.SetTile(coords, Resource.GroundTile);
        }
    }

    public override void OnDimensionSpawn()
    {
    }

    public override void OnDimensionDespawn()
    {
    }

    public override void OnComponentAttached()
    {
    }

    public override void OnComponentRemoved()
    {
    }
}