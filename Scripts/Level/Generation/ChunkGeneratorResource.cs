using Godot;
using System;

namespace VoxelGame.Generation
{
    public abstract class ChunkGeneratorResource : Resource
    {
        public abstract ChunkGenerator GetGeneratorInstance();
    }
}
