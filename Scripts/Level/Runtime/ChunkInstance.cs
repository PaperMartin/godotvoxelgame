using Godot;
using System;
using System.Collections.Generic;
using VoxelGame.Data;
using VoxelGame.Generation;
using VoxelGame.Runtime;

public class ChunkInstance : Spatial
{
    public Chunk chunk;

    private List<MeshGeneratorInstance> Generator;

    public override void _Ready()
    {
        base._Ready();
        chunk.OnChunkSpawn();
        
    }

    public override void _ExitTree()
    {
        base._ExitTree();
        chunk.OnChunkDespawn();
    }

    public void StartUpdateMesh()
    {
        
    }

    public void ForceMeshUpdateCompletion()
    {
        
    }

    public void GenerateChunk()
    {
        
    }

    public void ForceFinishChunkGen()
    {
        
    }
}
