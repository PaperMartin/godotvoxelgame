using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaperMartin.Utils.Maths;
using VoxelGame.Data;

namespace VoxelGame.Runtime
{
    public abstract class MeshGenerator : Resource
    {
        public abstract MeshGeneratorInstance GetGenerator(Chunk chunk);
    }

    public abstract class MeshGeneratorInstance : Spatial
    {
        public Chunk SourceChunk;
        
        public MeshInstance TargetChunkMeshInstance;
        public abstract void StartMeshUpdate();
        public abstract void WaitForCompletion();
    }
}
