using Godot;
using System;
using PaperMartin.Utils.Maths;
using VoxelGame.Data;

namespace VoxelGame.Runtime
{
    public interface IChunkProvider
    {
        void RequestChunk(Vector3Int coords);

        void RequestChunks(Vector3Int[] coords);

        Chunk WaitForChunk(Vector3Int coords);

        Chunk[] WaitForChunks(Vector3Int[] coords);
    }
}
