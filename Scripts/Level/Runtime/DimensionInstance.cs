using Godot;
using System;
using PaperMartin.Utils.Maths;
using System.Threading.Tasks;
using VoxelGame.Data;
using VoxelGame.Generation;

namespace VoxelGame.Runtime
{
    public class DimensionInstance : Node
    {
        [Export] private LevelSettings levelSettings;

        [Export] private DimensionSettings dimensionSettings;

        [Export] private ChunkGeneratorResource Generator;

        [Export] private PackedScene ChunkScene;

        private Dimension Dimension;

        private ChunkInstance[,,] ChunkInstances;

        private int Seed = 0;

        private ChunkGenerator ChunkGenComponent;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            //GenerateWorld();
        }

        public void Reseed()
        {
            RandomNumberGenerator rng = new RandomNumberGenerator();
            rng.Randomize();
            Seed = rng.RandiRange(0, 99999);
        }

        public void GenerateWorld()
        {
            ResetWorld();
            
            GD.Print("Creating dimension");
            //Create Dimension
            Dimension dimension = new Dimension(dimensionSettings,levelSettings);
            dimension.OnCreated();
            
            GD.Print("Creating Generator Component");
            ChunkGenComponent = Generator.GetGeneratorInstance();
            dimension.AttachComponent(ChunkGenComponent);
            ChunkGenComponent.StartGeneratingMap(Seed);
            
            
            Vector3Int MapSize = dimensionSettings.GetDimensionSize();
            ChunkInstances = new ChunkInstance[MapSize.x, MapSize.y, MapSize.z];

        }

        public void ForceFinishWorldGeneration()
        {
            if (ChunkGenComponent != null)
            {
                ChunkGenComponent.WaitForCompletion();
            }
        }

        public void GenerateChunkMeshes()
        {
            
        }

        
        
        public void ResetWorld()
        {
            GD.Print("Resetting world");
            foreach (Node childnode in GetChildren())
            {
                childnode.QueueFree();
            }

            if (Dimension != null)
            {
                ForceFinishWorldGeneration();
                Dimension.OnDespawn();
                Dimension = null;
            }
        }
        
        public float GetGenerationProgress()
        {
            if (ChunkGenComponent != null)
            {
                return ChunkGenComponent.GetProgress();
            }

            return 1f;
        }

        public float GetMeshProgress()
        {
            return 0;
        }

    }
}
