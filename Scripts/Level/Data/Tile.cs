using Godot;
using System;

namespace VoxelGame.Data
{
    public class Tile : Resource
    {
        [Export] public bool IsBlock = true;

        [Export] public bool OccludesAdjacentTiles = true;

        [Export] public Material BlockMaterial;
    }
}