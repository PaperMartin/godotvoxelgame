using Godot;
using System;
using PaperMartin.Utils.Maths;
using VoxelGame.Generation;

namespace VoxelGame.Data
{
    public class DimensionSettings : Resource
    {
        [Export()] private Vector3 WorldSize;

        public Vector3Int GetDimensionSize()
        {
            return WorldSize.RoundToInt();
        }
    }
}
