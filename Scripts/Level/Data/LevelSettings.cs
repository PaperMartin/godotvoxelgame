using Godot;
using System;
using PaperMartin.Utils.Maths;

namespace VoxelGame.Data
{
    public class LevelSettings : Resource
    {
        [Export()] private float BlockSize;
        [Export()] private Vector3 ChunkSize;


        public float GetBlockSize()
        {
            return BlockSize;
        }

        public Vector3Int GetChunkSize()
        {
            return ChunkSize.RoundToInt();
        }
    }
}