using Godot;
using System;
using VoxelGame.Data;

namespace VoxelGame.Data
{
    public abstract class DimensionComponent
    {
        public Dimension dimension;

        public virtual void OnDimensionSpawn()
        {
            
        }

        public virtual void OnDimensionDespawn()
        {
            
        }

        public virtual void OnComponentAttached()
        {
            
        }

        public virtual void OnComponentRemoved()
        {
            
        }
    }
}