using Godot;
using System;
using System.Collections.Generic;
using PaperMartin.Utils.Maths;
using System.Linq;

namespace VoxelGame.Data
{
    public class Dimension : LevelData
    {
        public DimensionSettings Settings;

        [Export()] private Chunk[,,] chunks;

        private List<DimensionComponent> Components;

        public System.Threading.Mutex mutex = new System.Threading.Mutex();
        

        public Chunk GetChunk(Vector3Int coords)
        {
            return chunks[coords.x, coords.y, coords.z];
        }

        public void AttachComponent(DimensionComponent component)
        {
            component.dimension = this;
            Components.Add(component);
        }
        
        public IEnumerable<Vector3> GetAllCoords()
        {
            Vector3Int DimSize = Settings.GetDimensionSize();
            return Enumerable.Range(0, DimSize.x).SelectMany(x =>
                Enumerable.Range(0, DimSize.y)
                    .SelectMany(y => Enumerable.Range(0, DimSize.z).Select(z => new Vector3(x, y, z))));
        }

        public void OnSpawn()
        {
            foreach (DimensionComponent component in Components)
            {
                component.OnDimensionSpawn();
            }
        }

        public void OnDespawn()
        {
            foreach (DimensionComponent component in Components)
            {
                component.OnDimensionSpawn();
            }
        }

        public void OnCreated()
        {
            
        }
        
        public Dimension(DimensionSettings dimensionSettings, LevelSettings levelSettings)
        {
            Settings = dimensionSettings;
            LevelSettings = levelSettings;
            Vector3Int dimSize = dimensionSettings.GetDimensionSize();
            chunks = new Chunk[dimSize.x, dimSize.y, dimSize.z];
            for (int x = 0; x < dimSize.x; x++)
            {
                for (int y = 0; y < dimSize.y; y++)
                {
                    for (int z = 0; z < dimSize.z; z++)
                    {
                        chunks[x, y, z] = new Chunk(levelSettings);
                    }
                }
            }

            if (Components == null)
            {
                Components = new List<DimensionComponent>();
            }
        }

        public Dimension()
        {
            if (Components == null)
            {
                Components = new List<DimensionComponent>();
            }
        }
    }
}