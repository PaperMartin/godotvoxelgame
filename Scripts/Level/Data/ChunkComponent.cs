using Godot;
using System;
using PaperMartin.Utils.Maths;

namespace VoxelGame.Data
{
    public abstract class ChunkComponent : Resource
    {
        public Chunk chunk;
        public abstract void OnComponentAdded();

        public abstract void OnTileRemoved(Vector3Int ChunkCoord, Tile tile);
        
        public abstract void OnTileAdded(Vector3Int ChunkCoord, Tile tile);

        public abstract void OnChunkSpawn();

        public abstract void OnChunkDespawn();
    }
}