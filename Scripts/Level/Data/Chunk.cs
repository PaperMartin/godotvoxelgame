using Godot;
using System;
using System.Collections.Generic;
using PaperMartin.Utils.Maths;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace VoxelGame.Data
{
    public class Chunk : LevelData
    {
        [Export()] private Tile[,,] Tiles;

        [Export()] public List<ChunkComponent> Components;

        public System.Threading.Mutex mutex { get; private set; }= new System.Threading.Mutex();

        public void SetTile(Vector3Int coords, Tile tile)
        {
            Tile currentTile = GetTile(coords);
            if (currentTile != null)
            {
                RemoveTile(coords);
            }

            if (tile != null)
            {
                AddTile(coords, tile);
            }
        }

        public Tile GetTile(Vector3Int coords)
        {
            if (coords.x >= 0 && coords.x < Tiles.GetLength(0) &&
                coords.y >= 0 && coords.y < Tiles.GetLength(1) &&
                coords.z >= 0 && coords.z < Tiles.GetLength(2))
            {
                return Tiles[coords.x, coords.y, coords.z];
            }


            else
            {
                //GD.Print("Tile out of bounds " + coords.x + " " + coords.y + " " + coords.z + " ");
                return null;
            }
        }

        private void RemoveTile(Vector3Int coords)
        {
            Tile currentTile = GetTile(coords);
            foreach (ChunkComponent component in Components)
            {
                component.OnTileRemoved(coords, currentTile);
            }
        }

        private void AddTile(Vector3Int coords, Tile newTile)
        {
            Tiles[coords.x, coords.y, coords.z] = newTile;
            foreach (ChunkComponent component in Components)
            {
                component.OnTileAdded(coords, newTile);
            }
        }

        public Chunk(LevelSettings chunklevelSettings)
        {
            LevelSettings = chunklevelSettings;
            Vector3Int size = LevelSettings.GetChunkSize();
            Tiles = new Tile[size.x, size.y, size.z];
            if (Components == null)
            {
                Components = new List<ChunkComponent>();
            }
        }

        public void OnChunkCreated()
        {
        }

        public void OnChunkSpawn()
        {
            foreach (ChunkComponent component in Components)
            {
                component.OnChunkSpawn();
            }
        }

        public void OnChunkDespawn()
        {
            foreach (ChunkComponent component in Components)
            {
                component.OnChunkDespawn();
            }
        }

        public Chunk()
        {
            if (Components == null)
            {
                Components = new List<ChunkComponent>();
            }
        }
    }
}